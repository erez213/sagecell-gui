import { createOp } from '../core/operation'
import { functionInput, textInput, numberInput, variableInput, arrayInput, textareaInput } from '../core/inputs'
import { functionOutput, imageOutput, arrayOutput, paraOutput, titleOutput } from '../core/outputs'

const titleOp = createOp(
    "title",
    "Add Title",
    [textInput("Title", "Enter the title")],
    (v, title) => { return "# " + title.value + "\n1"; },
    (title) => { return null; },
    titleOutput()
);
const paraOp = createOp(
    "paragraph",
    "Add Paragraph",
    [textareaInput("Paragraph", "Write notes inside this paragraph")],
    (v, para) => {
        return '"""\n' + para.value + '\n"""\n1'
    },
    (para) => { return null; },
    paraOutput()
);

let list = [
    titleOp, paraOp
]
export default { list };
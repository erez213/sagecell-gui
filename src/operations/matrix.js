import { createOp } from '../core/operation'
import { functionInput, textInput, numberInput, variableInput, arrayInput, matrixInput, martixnameInput } from '../core/inputs'
import { functionOutput, imageOutput, arrayOutput, matrixOutput, numberOutput } from '../core/outputs'
import { max } from 'lodash';

const declarMatrixOp = createOp(
    "declare_matrix",
    "Declare Matrix",
    [textInput("Name", "Enter the name of this matrix"), matrixInput("Matrix", "Enter the numbers and size of the matrix")],
    (v, name, func) => { return v + "=Matrix([" + func.value.map((e) => "[" + e.join(',') + "]").join(',') + "]);" + v; },
    (name, func) => { return null; },
    matrixOutput()
);

const determinanteOp = createOp(
    "determinant_matrix",
    "Find Determinant",
    [textInput("Name", "Enter the name of this operation"), martixnameInput("Matrix", "Enter the matrix name")],
    (v, name, matrix) => { return v + "=" + matrix.value + ".det();" + v; },
    (name, func) => { return null; },
    numberOutput()
);

const matrixscalarmultiplyOp = createOp(
    "matrix_scalar",
    "Multiply by scalar",
    [textInput("Name", "Enter the name of this operation"), martixnameInput("Matrix", "Enter the matrix name"), numberInput("Scalar", "Enter the nubmer to multiply the matrix"),],
    (v, name, matrix, scalar) => { return v + "=" + matrix.value + "*" + scalar.value + ";" + v; },
    (name, func) => { return null; },
    matrixOutput()
);

const matrixmulitplyOp = createOp(
    "matrix_matrix",
    "Multiply multiply",
    [textInput("Name", "Enter the name of this operation"), martixnameInput("Matrix", "Enter the first matrix name"), martixnameInput("Matrix", "Enter the second matrix name"),],
    (v, name, matrix1, matrix2) => { return v + "=" + matrix1.value + "*" + matrix2.value + ";" + v; },
    (name, func) => { return null; },
    matrixOutput()
);

let list = [
    declarMatrixOp, determinanteOp, matrixscalarmultiplyOp, matrixmulitplyOp

]
export default { list };
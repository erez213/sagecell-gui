import { createOp } from '../core/operation'
import { functionInput, textInput, numberInput, variableInput, arrayInput } from '../core/inputs'
import { functionOutput, imageOutput, arrayOutput } from '../core/outputs'
import { max } from 'lodash';

const declareFunctionOp = createOp(
    "declare_func",
    "Declare function",
    [textInput("Name", "Enter the name of this operation"), functionInput("Function", "Enter the function you wish to save")],
    (v, name, func) => { return v + "=" + func.value.ascii + ";" + v; },
    (name, func) => { return null; },
    functionOutput()
);
const cutPoint = createOp(
    "find_roots",
    "Find roots",
    [textInput("Name", "Enter the name of this operation"), functionInput("Function", "Enter the function you want to find her roots")],
    (v, name, func) => {
        return v + "=solve(" + func.value.ascii + ",x);" + v;
    },
    (name, func) => { return null; },
    arrayOutput()
);
const findDerivativeOp = createOp(
    "find_derivative",
    "Find derivative",
    [textInput("Name", "Enter the name of this operation"), functionInput("Function", "Enter the function you want to find derivative of")],
    (v, name, func) => { return v + "=derivative(" + func.value.ascii + ");" + v; },
    (func) => { return null; },
    functionOutput()
);
const MinimumMaximum = createOp(
    "minimum_maximum",
    "Find Minimum & Maximum",
    [
        textInput("Name", "Enter the name of this operation"),
        functionInput("Function", "Enter the function you want to find her maximum & minimum")
    ],
    (v, name, func) => {
        let command = "g=derivative(" + func.value.ascii + ");";
        return command + v + "=solve(g,x);" + v;
    },
    (func) => { return null; },
    arrayOutput()
);
const drawPlotOp = createOp(
    "plot_single",
    "Draw Plot one function",
    [
        textInput("Name", "Enter the name of this operation"),
        functionInput("Function", "Enter the function you want to show plot"),
        numberInput("Min X", "Enter the minimum X for the plot"),
        numberInput("Max X", "Enter the maximum X for the plot"),
        numberInput("Min Y", "Enter the minimum X for the plot"),
        numberInput("Max Y", "Enter the maximum y for the plot"),
    ],
    (v, name, func, minx, maxx, miny, maxy) => { return "g = Graphics(); g += plot(" + func.value.ascii + ",(x," + minx.value + "," + maxx.value + "),ymin=" + miny.value + ",ymax=" + maxy.value + "); g.show();" },
    (func) => { return null; },
    imageOutput()
);
const drawPlotMultipleOp = createOp(
    "plot_multiple",
    "Show Plot multiple functions",
    [
        textInput("Name", "Enter the name of this operation"),
        arrayInput("Functions", "Enter the functions you want to show plot", functionInput("Function {i}", "")),
        numberInput("Min X", "Enter the minimum X for the plot"),
        numberInput("Max X", "Enter the maximum X for the plot"),
        numberInput("Min Y", "Enter the minimum X for the plot"),
        numberInput("Max Y", "Enter the maximum y for the plot"),
    ],
    (v, name, func, minx, maxx, miny, maxy) => {
        let command = "g = Graphics(); ";
        for (var i = 0; i < func.value.length; i++) {
            command += "g += plot(" + func.value[i].value.ascii + ",(x," + minx.value + "," + maxx.value + "),ymin=" + miny.value + ",ymax=" + maxy.value + ");";
        }
        command += " g.show();";
        return command;
    },
    (func) => { return null; },
    imageOutput()
);
const findIntegralOp = createOp(
    "find_integral",
    "Find integral",
    [textInput("Name", "Enter the name of this operation"), functionInput("Function", "Enter the function you want to find integral of")],
    (v, name, func) => { return v + "=integral(" + func.value.ascii + ", x);" + v; },
    (func) => { return null; },
    functionOutput()
);

let list = [
    declareFunctionOp, cutPoint, findDerivativeOp, MinimumMaximum, drawPlotOp, drawPlotMultipleOp, findIntegralOp

]
export default { list };
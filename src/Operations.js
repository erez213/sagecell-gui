import functions from "./operations/functions"
import text from "./operations/text"
import matrix from "./operations/matrix"

let list = [
    {
        name: "Functions",
        list: functions.list
    },
    {
        name: "Matrix",
        list: matrix.list
    },
    {
        name: "Text",
        list: text.list
    },
];
export default { list }
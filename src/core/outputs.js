const asciimath2latex = require('asciimath-to-latex')
const nl2br = require('nl2br');

//Create function input
const basicOutput = () => {
    return { type: "basic", format(inputs, data) { return data.data; } };
};

export const functionOutput = () => {
    let o = basicOutput();
    o.type = "func";
    o.format = function (inputs, data) {
        return asciimath2latex(data.data);
    };
    return o;
}

export const numberOutput = () => {
    let o = basicOutput();
    o.type = "number";
    o.format = function (inputs, data) {
        return data.data;
    };
    return o;
}

//Create image input
export const imageOutput = () => {
    let o = basicOutput();
    o.type = "image";
    o.format = function (inputs, data) {
        return "<img style='width:100%;' src='" + data.data + "' />";
    }
    return o;
}

//Create array output
export const arrayOutput = () => {
    let o = basicOutput();
    o.type = "array";
    return o;
}

//Create title output
export const titleOutput = () => {
    let o = basicOutput();
    o.type = "title";
    o.format = function (inputs, data) {
        return "<h1>" + inputs[0].value + "</h1>";
    }
    return o;
}
//Create paragraph output
export const paraOutput = () => {
    let o = basicOutput();
    o.type = "paragraph";
    o.format = function (inputs, data) {
        return "<p>" + inputs[0].value.replace(/(?:\r\n|\r|\n)/g, '<br>') + "</p>";
    }
    return o;
}


//Create paragraph output
export const matrixOutput = () => {
    let o = basicOutput();
    o.type = "matrix";
    o.format = function (inputs, data) {
        return nl2br(data.data);
    }
    return o;
}

import * as _ from "lodash";
import Vue from "vue";

const basicInput = (name, info) => {
    return {
        name: name,
        info: info,
        value: null,
        getDefault() {
            return "";
        },
        print() {
            return this.name + " = " + this.value;
        },
        validate() {
            if (this.value == null || this.value == "") {
                return this.name + " can't be empty";
            }
            return null;
        },
        setId(i) {
            this.name = this.name.replace("{i}", i + 1);
            return this;
        }
    }
}

//Create function input
export const functionInput = (name, info) => {
    let c = basicInput(name, info);
    c.type = "function";
    c.getDefault = function () {
        return { latex: "", ascii: "" };
    };
    c.print = function () {
        return this.name + " = " + this.value.latex;
    };

    return c;
}

//Create raw text input
export const textInput = (name, info) => {
    let c = basicInput(name, info);
    c.type = "text";

    return c;
}

//Create raw text input
export const textareaInput = (name, info) => {
    let c = basicInput(name, info);
    c.type = "textarea";

    return c;
}


//Create number input
export const numberInput = (name, info) => {
    let c = basicInput(name, info);
    c.type = "number";
    c.validate = function () {
        if (this.value == null || this.value == "") {
            return this.name + " can't be empty";
        }
        if (this.value.match(/^\d+\.\d+$/)) {
            return this.name + " must be valid number";
        }
        return null;
    }
    return c;
}

//Create variable input
export const variableInput = (name, info) => {
    let c = basicInput(name, info);
    c.type = "variable";
    c.validate = function () {
        if (this.value == null || this.value == "") {
            return this.name + " can't be empty";
        }
        if (this.value.length > 1) {
            return this.name + " can only be one char";
        }
        return null;
    }
    return c;
}

//Create function input
export const arrayInput = (name, info, items) => {
    let c = basicInput(name, info);
    c.type = "array";
    c.validate = function () {
        if (this.value == null) {
            return this.name + " can't be empty";
        }
        if (this.value.length == 0) {
            return this.name + " must have at least one element";
        }
        for (var i = 0; i < this.value.length; i++) {
            let c = this.value[i].validate();
            if (c != null) return c;
        }
        return null;
    }
    c.getDefault = function () {
        return [];
    };
    c.print = function () {
        return this.value.map((i) => i.print());
    };
    c.items = items;

    //Functions for array
    c.new = function () {
        let length = this.value.push(_.cloneDeep(this.items));
        Vue.set(this.value[length - 1], "value", this.items.getDefault());
    };
    c.removeInPos = function (i) {
        console.log(this.value);
        //this.value.splice(i, 1);
        Vue.delete(this.value, i);
    };
    return c;
}

//Create raw text input
export const matrixInput = (name, info) => {
    let c = basicInput(name, info);
    c.type = "matrix";
    c.getDefault = function () {
        return [["1", "0", "0"], ["0", "1", "0"], ["0", "0", "1"]];
    };
    c.addRow = function () {
        this.value.push(this.value[0].map((e) => null));
    };

    c.addColumn = function () {
        for (let i = 0; i < this.value.length; i++) {
            this.value[i].push(null);
        }
    };

    c.deleteRow = function (i) {
        if (this.value.length == 1) return;
        Vue.delete(this.value, i);
    }

    c.deleteColumn = function (col) {
        if (this.value[0].length == 1) return;

        for (let i = 0; i < this.value.length; i++) {
            Vue.delete(this.value[i], col);
        }
    }

    c.validate = function () {
        for (let i = 0; i < this.value.length; i++) {
            for (let j = 0; j < this.value[i].length; j++) {

                if (this.value[i][j] == null || this.value[i][j] == "" || this.value[i][j].match(/^\d+\.\d+$/)) {
                    return "[" + i + "][" + j + "] isn't valid number."
                }
            }
        }
        return null;
    }

    c.print = function () {
        return this.value.map((i) => "[" + i.join(',') + "]");
    };
    return c;
}

//Create raw text input
export const martixnameInput = (name, info) => {
    let c = basicInput(name, info);
    c.type = "matrixname";

    return c;
}
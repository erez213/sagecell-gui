import Vue from "vue";
export const createOp = (id, name, inputs, builder, validator, output) => {
    return {
        id: id,
        key: null,
        name: name,
        inputs: inputs,
        builderFunc: builder,
        validatorFunc: validator,
        outputType: output,
        outputData: null,
        error: null,

        //Validate input of the operation
        validate() {
            for (var i = 0; i < this.inputs.length; i++) {
                let v = this.inputs[i].validate();
                if (v != null) return v;
            }
            let v = this.validatorFunc(...this.inputs);
            if (v != null) return v;
            return null;
        },

        //Build the Sage expression
        build() {
            return this.builderFunc(this.key, ...this.inputs);
        },

        //Return pretty Sage expression
        prettyBuild() {
            return this.build().split(';').filter((r) => {
                return r.trim() != this.key
            }).map((r) => r.trim()).join("\n").replace(/\n1$/, "")
        },
        //Return pretty Sage expression
        prettyFullBuild() {
            return this.build().split(';').map((r) => r.trim()).join("\n").replace(/\n1$/, "");
        },

        //Print operation input fields
        print() {
            return this.inputs.filter((input) => input.name != "Name").map((input) => input.print());
        },

        //Print output
        output() {
            if (!this.hasOutput()) return "No output";
            return this.outputType.format(this.inputs, this.outputData);
        },
        isTextOutput() {

            if (this.outputType.type == "image") return false;
            if (this.outputType.type == "title") return false;
            if (this.outputType.type == "matrix") return false;
            if (this.outputType.type == "paragraph") return false;
            return true;
        },
        hasOutput() {
            return this.outputData != null;
        },
        setOutput(output) {
            this.error = null;
            this.outputData = output;
        },

        //Generate random key for the operation
        generateKey(id) {
            this.key = this.outputType.type + "" + id;
        },

        //Make reactive for Vue
        makeReactive() {
            for (var i = 0; i < this.inputs.length; i++) {
                Vue.set(this.inputs[i], "value", this.inputs[i].getDefault());
            }
        },

        //Set error value
        setError(err) {
            this.outputData = null;
            this.error = err;
        },
        hasError() {
            return this.error != null;
        },
        outputError() {
            return this.error.ename + ": " + this.error.evalue;
        },

        getName() {
            if (this.inputs[0].name != "Name") return null;
            return this.inputs[0].value;
        }
    }
};
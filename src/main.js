import Vue from 'vue'
import App from './App'
import Store from './Store'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueMathjax from 'vue-mathjax'
import MathLive from "mathlive";
import Mathfield from "./mathlive/vue-mathlive";
import DynamicInput from "./components/DynamicInput";
import OpSelector from "./components/OpSelector";

Vue.use(Mathfield, MathLive);

Vue.use(VueMathjax)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.component('dynamic-input', DynamicInput)
Vue.component('op-selector', OpSelector)

new Vue({
  el: '#app',
  Store,
  template: '<App/>',
  components: {
    App
  },
  created() {
    document.title = 'SageMath GUI';

    var scripts = [
      "https://polyfill.io/v3/polyfill.min.js?features=es6",
      "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"
    ];
    scripts.forEach(script => {
      let tag = document.createElement("script");
      tag.setAttribute("src", script);
      document.head.appendChild(tag);
    });
  }
})